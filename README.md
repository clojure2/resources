# resources

> Clojure/Clojurescript resourcess

* [Simple Made Easy by Rich Hickey](https://www.infoq.com/presentations/Simple-Made-Easy)
* [Effective Programs - 10 Years of Clojure](https://www.youtube.com/watch?v=2V1FtfBDsLU)


## Tutorials

* http://clojure-doc.org/articles/content.html
* https://clojure.org/api/cheatsheet
* https://www.braveclojure.com/
* 

...


## Libraries

* https://github.com/riemann/riemann
* https://github.com/omcljs/om
* 


## Tools

* https://github.com/bhauman/lein-figwheel
* 


## Machine Learning

* https://github.com/originrose/cortex
* https://github.com/aria42/flare
* https://github.com/aria42/koala
* https://github.com/joeddav/devol
* 

* http://course.fast.ai/
* 



